(ns hwo2014bot.core
  (:require [clojure.data.json :as json]
            [clojure.pprint :as p])
  (:use [aleph.tcp :only [tcp-client]]
        [lamina.core :only [enqueue wait-for-result wait-for-message]]
        [gloss.core :only [string]])
  (:gen-class))



(def pieces)
(def total-pieces)
(def total-lanes)
(def lanes)
(def cars)
(def my-car)
(def prev-throttle)
(def prev-car-pos)

(defn- json->clj [string]
  (json/read-str string :key-fn keyword))

(defn send-message [channel message]
  (enqueue channel (json/write-str message)))

(defn read-message [channel]
  (json->clj
    (try
      (wait-for-message channel)
      (catch Exception e
        (println (str "ERROR: " (.getMessage e)))
        (System/exit 1)))))

(defn connect-client-channel [host port]
  (wait-for-result
   (tcp-client {:host host,
                :port port,
                :frame (string :utf-8 :delimiters ["\n"])})))

(defmulti handle-msg :msgType)

(defn log-msg [msg]
  (case (:msgType msg)
    "join" (do (println "Joined"))
    "gameInit" (do (println "Game Init"))
    "yourCar" (do (println "Car info"))
    "gameStart" (do (println "Race started"))
    ;;"crash" (do (println "Someone crashed") (println "car pos:") (p/pprint prev-car-pos) (println "prev throttle:")(p/pprint prev-throttle))
    "gameEnd" (do (println "Race ended"))
    "error" (do (println (str "ERROR: " (:data msg))))
    :noop))

(defn log-msg-detailed [msg]
  (case (:msgType msg)
    "join" (do (println "Joined") (p/pprint msg))
    "gameInit" (do (println "Game Init") (p/pprint msg))
    "yourCar" (do (println "Car info") (p/pprint msg))
    "gameStart" (do (println "Race started") (p/pprint msg))
    "carPositions" (do (println "Car positions") (p/pprint msg))
    "crash" (do (println "Someone crashed") (p/pprint msg))
    "gameEnd" (do (println "Race ended") (p/pprint msg))
    "error" (do (println (str "ERROR: " (:data msg))) (p/pprint msg))
    :noop))

(defn game-loop [channel]
  (let [msg (read-message channel)]
    (log-msg msg)
    (send-message channel (handle-msg msg))
    (recur channel)))

(defn -main[& [host port botname botkey]]
  (let [channel (connect-client-channel host (Integer/parseInt port))]
    (send-message channel {:msgType "join" :data {:name botname :key botkey}})
    (game-loop channel)))


(defn my-position [msg]
  (loop [positions (:data msg)
         car (peek positions)]
    (if (= (:color my-car) (get-in car [:id :color]))
      car
      (recur (pop positions) (peek (pop positions))))
    )
  )




(defn throttle [ curPiece nextPiece car-pos]
  (let [inPieceDist (get-in car-pos [:piecePosition :inPieceDistance])
        angle (:angle car-pos)]
    (cond
      ;; (and (:length curPiece) (:length nextPiece)) {:msgType "throttle" :data 1.0}
      ;; (and (:length curPiece) (:angle nextPiece) (< (/ inPieceDist (:length curPiece)) 0.6) (<= angle 6) ) {:msgType "throttle" :data 0.5}
      ;; (and (:length curPiece) (:angle nextPiece) (< (/ inPieceDist (:length curPiece)) 0.6) (> angle 6) ) {:msgType "throttle" :data 0.5}
      ;; (and (:length curPiece) (:angle nextPiece) (> (/ inPieceDist (:length curPiece)) 0.6) ) {:msgType "throttle" :data 0.4}


      (or  (> angle 6.0 ) (< angle -6.0 )) {:msgType "throttle" :data 0.4}

      (and  (:angle curPiece)) {:msgType "throttle" :data 0.7}

      :default {:msgType "throttle" :data 1.0}
      )
    ))







;gather car info
(defmethod handle-msg "yourCar" [msg]
  (println "getting car info")
  (def my-car (:data msg))
  {:msgType "ping" :data "ping"})

;gather track info
(defmethod handle-msg "gameInit" [msg]
  (def pieces (get-in msg [:data :race :track :pieces]))
  (def total-pieces (count pieces))
  (def cars (get-in msg [:data :race :track :cars]))
  (def lanes (get-in msg [:data :race :track :lanes]))
  (def total-lanes (count lanes))

  (println "pieces:")
  (p/pprint pieces)

  (println "lanes:")
  (p/pprint lanes)

  {:msgType "ping" :data "ping"})

;gather car info
(defmethod handle-msg "gameStart" [msg]
  {:msgType "throttle" :data 1.0})

(defmethod handle-msg "carPositions" [msg]
  (let [car (my-position msg)
        piece-index (get-in car [:piecePosition :pieceIndex])
        current-piece (pieces piece-index)
        next-piece (pieces (if (> (inc piece-index) (dec total-pieces))
                             0
                             (inc piece-index)))
        new-throttle (throttle current-piece next-piece car)]

    (def prev-throttle new-throttle)
    (def prev-car-pos car)
    new-throttle


    )


  ;{:msgType "throttle" :data 0.5}
  )

(defmethod handle-msg "crash" [msg]
  {:msgType "ping" :data "ping"})

(defmethod handle-msg :default [msg]
  {:msgType "ping" :data "ping"})




(-main "webber.helloworldopen.com" "8091" "daury" "fNMnx5/KyKG2UA")
